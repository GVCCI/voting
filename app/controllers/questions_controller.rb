class QuestionsController < ApplicationController

  before_action :authenticate_user!

  # def new
  #   @question = Question.new
  # end

  # def create
  #   @question = Question.create(question_params)
  #   if @question.save
  #     flash[:succsess] = "Question created"
  #   else
  #     render 'new'
  #   end
  # end

  def upvote
    if session[:voted_at] == nil || session[:voted_at] < 24.hours.ago
      session[:voted_at] = nil
      session[:voted] = false
    end
    if session[:voted] == false
      @question = Question.find(params[:id])
      @question.increment!(:votes_count)
      session[:voted_id] = @question.id
      session[:voted] = true
      session[:voted_at] = Time.now
      redirect_to :back
    else
      redirect_to :back
      flash[:error] = "You have already voted today!"
    end
  end

  def revote
    @question = Question.find(session[:voted_id])
    @question.decrement!(:votes_count)
    session[:voted_at] = nil
    session[:voted] = false
    redirect_to :back
  end

  # private

  #   def question_params
  #     params.require(:question).permit(:title, :comment, :votes_count)
  #   end

end
