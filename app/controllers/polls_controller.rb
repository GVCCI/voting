class PollsController < ApplicationController

  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :newquestion]

  def index
    @polls = Poll.all
  end
  
  def show
    @poll = Poll.find(params[:id])
  end

  def new
    @poll = Poll.new
    3.times { @poll.questions.build }
  end
  
  def create
    @poll = Poll.create(poll_params)
    if @poll.save
      redirect_to poll_path(@poll)
      flash[:succsess] = "Poll created!"
    else
      render 'new'
    end
  end

  def edit
    @poll = Poll.find(params[:id])
  end

  def update
    @poll = Poll.find(params[:id])
    @poll.update_attributes(poll_params)
    if @poll.errors.empty?
      redirect_to poll_path(@poll)
      flash[:succsess] = "Poll updated!"
    else
      render 'edit'
    end
  end

  def newquestion
    @poll = Poll.find_by(params[:poll])
    question = Question.create(title: nil, comment: nil)
    @poll.questions << question
    redirect_to :back
  end

    private

      def poll_params
        params.require(:poll).permit(:name, questions_attributes:  [:id, :title, :comment, :_destroy])
      end

end
