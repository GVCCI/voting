class Question < ActiveRecord::Base

  belongs_to :poll
  default_scope -> { order('votes_count DESC') }

end
