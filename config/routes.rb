Voting::Application.routes.draw do
  
  root 'main#index'

  devise_for :users, controllers: { registrations: "registrations" }

  devise_scope :user do
    get 'login' => 'devise/sessions#new'
    get 'join' => 'devise/registrations#new'
    get 'settings' => 'devise/registrations#edit'
  end

  resources :polls do
    get :newquestion, on: :member
  end
  
  resources :questions do
    get :upvote, on: :member
    get :revote, on: :member
  end

end
